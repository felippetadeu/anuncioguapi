import 'dart:convert';

import 'package:anuncio_guapi/constantes.dart';
import 'package:anuncio_guapi/models/anuncio.dart';
import 'package:anuncio_guapi/pages/anunciogaleria.dart';
import 'package:anuncio_guapi/requests/anunciorequest.dart';
import 'package:flutter/material.dart';
import 'package:badge/badge.dart';

class HomePage extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return new HomePageState();
  }
}

class HomePageState extends State<HomePage>{

  final AnuncioRequest anuncioRequest = new AnuncioRequest();
  bool isLoading = false;

  Align returnLikeButton(DTOAnuncio anuncio){
    return Align(
      alignment: Alignment.centerLeft,
      child: GestureDetector(
        onTap: (){
          setState(() {
            isLoading = true;
            anuncioRequest.getAnunciosSegmento(0);
            isLoading = false;
          });
        },
        child: Row(
          children: <Widget>[
            Badge.after(
              color: Colors.blueAccent,
              value: anuncio.QtdLike.toString(),
              child: Row(
                children: [
                  Padding(padding: EdgeInsets.all(7.0), child: Icon(Icons.thumb_up)),
                  Padding(padding: EdgeInsets.all(7.0), child: Text('Gostei',style: TextStyle(fontSize: 18.0)))
                ]
              )
            )
          ],
        ),
      ),
    );
  }

  Align returnUnlikeButton(DTOAnuncio anuncio){
    return Align(
      alignment: Alignment.centerRight,
      child: GestureDetector(
        onTap: (){
          setState(() {
            isLoading = true;
            anuncioRequest.getAnunciosSegmento(0);
            isLoading = false;
          });
        },
        child: Row(
          children: <Widget>[
            Badge.before(
              value: anuncio.QtdUnlike.toString(),
              child: Row(
                children: [
                  Padding(padding: EdgeInsets.all(7.0), child: Icon(Icons.thumb_down)),
                  Padding(padding: EdgeInsets.all(7.0), child: Text('Não Gostei',style: TextStyle(fontSize: 18.0))),
                ]
              )
            ),
          ],
        ),
      ),
    );
  }

  Widget returnCard(DTOAnuncio anuncio, BuildContext ctx){
    return Card(
      child: Column(
        children: <Widget>[
          Text(anuncio.Titulo, 
            textAlign: TextAlign.center, 
            style: TextStyle(
              fontSize: 18.0
            )
          ),
          GestureDetector(
            onTap: (){
              if (anuncio.PossuiGaleria){
                Navigator.push(context, MaterialPageRoute(builder: (context) => AnuncioGaleria(anuncio: anuncio,)));
              } else {
                Scaffold.of(ctx).showSnackBar(
                  SnackBar(
                    content: Text("Esse anúncio não possui galeria de imagens.")
                  )
                );
              }
            },
            child: (anuncio.Imagem != null ? Image.network(Constantes.Url + anuncio.Imagem) : CircleAvatar()),
          ),
          Text(
            anuncio.NomeEmpresa,
            textAlign: TextAlign.center
          ),
          Padding(
            padding: EdgeInsets.all(7.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                returnLikeButton(anuncio),
                returnUnlikeButton(anuncio)
              ],
            )
          )
        ],
      ),
    );
  }

  List<DropdownMenuItem<int>> dropItems = new List<DropdownMenuItem<int>>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Anúncios Guapi")
      ),
      body: Container(
            child: FutureBuilder(
              future: anuncioRequest.getAnunciosSegmento(0),
              builder: (context, snapshot) {
                if (snapshot.hasData){
                  List lista = json.decode(snapshot.data.body.toString());
                  return ListView.builder(
                    itemCount: lista.length,
                    itemBuilder: (context, indice){
                      var t = lista[indice];
                      DTOAnuncio a = new DTOAnuncio();
                      a.AnuncioId = t['AnuncioId'];
                      a.Descricao = t['Descricao'];
                      a.EmpresaId = t['EmpresaId'];
                      a.Imagem = t['Imagem'];
                      a.NomeEmpresa = t['NomeEmpresa'];
                      a.Titulo = t['Titulo'];
                      a.QtdLike = t['QtdLike'];
                      a.QtdUnlike = t['QtdUnlike'];
                      a.PossuiGaleria = t['QtdGaleria'] > 0;
                      return Padding(
                        padding: EdgeInsets.all(12),
                        child: returnCard(a, context)
                      );
                    },
                  );
                } else if (snapshot.hasError){
                  return Text(snapshot.error);
                }
                return CircularProgressIndicator();
              },
            ),
      ),
    );
  }
}